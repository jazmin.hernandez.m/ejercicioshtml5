var contenido = document.querySelector('#contenidoT');
var contenido = document.querySelector('#name');

document.body.onload = datosApi;

function datosApi(){
	fetch('http://dummy.restapiexample.com/api/v1/employees')
	.then(res => res.json())
	.then(datos => { tabla(datos);});
}

function tabla(datos){
	for(let i of datos.data){
		contenidoT.innerHTML += `
			<tr>
			    <td> ${i.id} </td>
		        <td> ${i.employee_name} </td>
		        <td> ${i.employee_salary} </td>
		        <td> ${i.employee_age} </td>
		    </tr>
			`
		}
	}

class ClearButton extends HTMLElement{
	constructor(){
		super();
		this.clear = "Clear";
	}

	connectedCallback(){
		let a = document.createElement("button");
		a.innerHTML = this.clear;
		this.appendChild(a);

		a.addEventListener('click', this.limpiar);
	}

	limpiar (){
		document.getElementById("filterT").value= " ";
	}
}


class HeaderV extends HTMLElement{
	constructor(){
		super();
		this.cabecera = 'View: SamplePage'
	}

	connectedCallback(){
		let shadowRoot = this.attachShadow({mode: 'open'});
		shadowRoot.innerHTML = `
        <style>
		div{
			background-color: #008080;
			width: 100%;
			height: 50px;
			font-size: 20px;
		}
        </style>

        <div class="estilo">View: SamplePage</div>
        `;
	}

}

window.customElements.define('clear-button', ClearButton);
window.customElements.define('div-view', HeaderV);


(function(document) {

      var busquedaTabla = (function(busqueda) {
      var input;

        function inputEvent(e) {
          input = e.target;
          var tables = document.getElementsByClassName(input.getAttribute('data-table'));
          busqueda.forEach.call(tables, function(table) {
          busqueda.forEach.call(table.tBodies, function(tbody) {
          busqueda.forEach.call(tbody.rows, filter);});
          });
        }

        function filter(row) {
          var text = row.textContent.toLowerCase(); 
          var val = input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            busqueda.forEach.call(inputs, function(input) {
              input.oninput = inputEvent;});
          }
        };
      })(Array.prototype);

      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          busquedaTabla.init();}
      });
    })
(document);





