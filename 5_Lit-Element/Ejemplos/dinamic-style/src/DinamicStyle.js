import { html, css, LitElement } from 'lit-element';

import { classMap } from 'lit-html/directives/class-map';
import { styleMap } from 'lit-html/directives/style-map';

export class DinamicStyle extends LitElement {
  static get styles() {
    return css`
      .mydiv{ 
        background-color: blue;
      }
      .someclass { 
        border: 1px solid red;
        }
    `;
  }

  static get properties() {
    return {
      classes: { type: Object},
      styles: { type: Object}
    };
  }

  constructor() {
    super();
    this.classes = {mydiv: true, someclass: true };
    this.styles = { color: 'green', fontFamily: 'Roboto'};
  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <div class="${classMap(this.classes)}" style=${styleMap(this.styles)}>
      some content
      </div>
    `;
  }

}
