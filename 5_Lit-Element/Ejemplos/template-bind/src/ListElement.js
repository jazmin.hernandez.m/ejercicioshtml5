import { html, css, LitElement } from 'lit-element';

export class ListElements extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--template-bind-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      items: {type: Array},
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <ul>
        ${this.items.map(item => html`<li>${items}</li>`)}
      </ul>
    `;
  }
}

