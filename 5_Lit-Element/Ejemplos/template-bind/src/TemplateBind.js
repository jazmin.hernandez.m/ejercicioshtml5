import { html, css, LitElement } from 'lit-element';
import './ListElement';
import './ColorList';

export class TemplateBind extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--template-bind-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      prop1: {type: String},
      prop2: {type: String},
      prop3: {type: Boolean},
      prop4: {type: String},
    };
  }

  constructor() {
    super();
    this.prop1 = 'text binding';
    this.prop2 = 'mydiv';
    this.prop3 = true;
    this.prop4 = 'pie';
  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <div>${this.prop1}</div>

      <div id="${this.prop2}">attribute binding</div>

      <div>
        boolean attribute binding
        <input type="text" ?disabled="${this.prop3}"/>
      </div>

      <div>
        property binding
        <input type="text" .value="${this.prop4}"/>
      </div>

      <div>event handler binding
        <button @click="${this.clickHandler}">click</button>
      </div>
    `;
  }
  clickHandler(e) {
    console.log(e.target);
  }
}

