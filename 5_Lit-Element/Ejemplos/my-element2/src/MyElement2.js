import { html, css, LitElement } from 'lit-element';
import './my-contact.js';

export class MyElement2 extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--my-element2-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      nombre: {type: String},
      email: {type: String},
      verMas: {type: Boolean},
      contactos:{type: Array}
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;

    this.contactos= [
      {
        nombre: 'Lucho Godinez',
        email: 'user1@gmail.com'
      },
      {
        nombre: 'Hugo Sanchez',
        email: 'user2@gmail.com'
      },
      {
        nombre: 'Jhon Doe',
        email: 'user3@gmail.com'
      }
    ];

  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <style>
        div {
          border: 1px solid black;
          padding: 10px;
          border-radius: 5px;
          display: inline-block;
        }
        h1{
          font-size: 1.2em;
          font-weight: normal;
        }
      </style>
        
      <div>
      ${this.contactos.map(contact => html`<my-contact nombre="${contact.nombre}" email="${contact.email}"></my-contact>`)}
      </div>
    `;
  }

}
