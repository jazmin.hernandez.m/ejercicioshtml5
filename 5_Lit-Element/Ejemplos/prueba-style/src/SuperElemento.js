import { html, css, LitElement } from 'lit-element';


export class SuperElemento extends LitElement {
  static get styles() {
    return css`
      button { 
        width: 50%; 
        font-style: italic;
      }
    `;
  } 
  render() {
    return html`
      <h2> Herencia de estilos </h2>
      <button>click</button>
    `;
  }
}