import { html, css, LitElement } from 'lit-element';
import { SuperElemento } from './SuperElemento.js';
import { HerenciaElemento } from './HerenciaElemento.js';

window.customElements.define('super-elemento', SuperElemento);
window.customElements.define('herencia-elemento', HerenciaElemento);

export class PruebaStyle extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--prueba-style-text-color, #000);
      }
    `;
  }
}
