import { html, css, LitElement } from 'lit-element';


export class FuncionamientoWish extends LitElement {
  static get styles() {
    return css`
      
    `;
  } 
  static get properties() {
    return {
      newV: { type: Array },
      task: { type: String },
      activo: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.newV = [];
    this.task = '';
    this.activo = false;
  }

  render() {
    return html`
    <div class="contenedor">
    <div class="principal">
      <h2>My WishList</h2>
      <fieldset class="texto">
      <legend>New Wish</legend>
        <input type="text" value="${this.task}" @change="${this.updateWishlist}">
      </fieldset>
      <div class="lista">${this.newV.map(news => html` 
        <div>
          <li>
          <input type="checkbox" 
          ?checked="${news.activo}" 
          @change="${ e => this.doChange(news, e.target.checked)}">${news.task}</li></div>`)}
        </div>
        <button @click="${this.deleteWish}">Delete</button>
        <button theme="primary" @click="${this.addWish}">Archive done</button>
        
      </div>
    </div>
      
     
    `;
  }

  updateWishlist(e) {
    this.task = e.target.value; 
  }
  addWish(e) {
    if (this.task) {
      this.newV = [...this.newV, { task: this.task, activo: false}];
      this.task = ''; 
      console.log(this.newV);
    }
  }
  doChange(wish, activo) {
    this.newV = this.newV.map(news =>
      wish === news ? { ...wish, activo } : news
    );
  }
  deleteWish() { 
    this.newV = this.newV.filter(news => !news.activo);
  }
}
