import { css } from 'lit-element';
import { FuncionamientoWish } from "./FuncionamientoWish";

export class HerenciaEstilos extends FuncionamientoWish {

  static get styles() {
    return [
      super.styles,
      css`
      :host {
        display: block;
        padding: 25px;
        color: var(--todo-view-text-color, #000);
      }
      div.contenedor{
        margin: 20px auto;
        width: auto;
        height: auto;

      }
      div.principal{
        border: 3px solid #00BFFF;
        background-color: #F5F5F5;
          border-radius: 10px;
          width: 400px;
          height: 450px;
          margin: 10px auto;
      }
      fieldset.texto{
        border: 1px solid gray;
        height: 50px;
        width:89%;
        margin-left:9px;
      }
      div.lista{
        margin-left:9px;
        margin-top:20px;
        height: 220px;
        width:95%;
        overflow: scroll;
        background-color: white;
      }
      input[type="text"]{
        margin: 10px auto;
        margin-left:5px;
        width: 95%;
      }
      h2{
        text-align:center;
        color: #00BFFF;
      }
      li{
        margin-left: 30px;
      }
      button{
        background-color: #00BFFF;
        border-radius: 5px;
        margin-right:10px;
        margin-top:20px;
        height: 25px;
        width: 120px;
        float:right;
      }`
    ];
  } 
}