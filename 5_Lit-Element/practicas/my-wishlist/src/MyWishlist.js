import { html, css, LitElement } from 'lit-element';
import { FuncionamientoWish } from './FuncionamientoWish.js';
import { HerenciaEstilos } from './HerenciaEstilos.js';

window.customElements.define('funcionamiento-wish', FuncionamientoWish);
window.customElements.define('herencia-estilos', HerenciaEstilos);

export class MyWishlist extends LitElement {
  static get styles() {
    return css`
      
    `;
  }
}