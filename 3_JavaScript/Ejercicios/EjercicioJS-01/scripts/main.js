//Ejercicios JavaScript 01

//Ejercicio 1
function f(x, y=2, z=7){
	return x+y+z;
}
console.log(f(5,undefined));


//Ejercicio 2
var animal = 'kitty';
var result = (animal === 'kitty') ? 'cute' : 'still nice';
console.log(result);


//Ejercicio 3
var animal = 'kitty';
var result = '';
if(animal === 'kitty'){
	result = 'cute';
} else{
	result = 'still nice';
}
console.log(result);


//Ejercicio 4
var a = 0;
var str = 'not a';
var b = '';
b = a === 0 ? (a = 1, str += 'test') : (a = 2);

console.log(b, a);


//Ejercicio 5
var a = 1;
a === 1 ? alert('Hey, it is 1!') : 0;


//Ejercicio 6
var a = 1;
a === 1 ? alert('Hey, it is 1!') : alert('Weird, what could it be?');
if (a === 1){
	alert('Hey, it is 1!')
}  else {
	alert('Weird, what could it be?')
}


//ERROR
//Ejercicio 7

var animal = 'kitty';
for (var i = 0; i < 5; ++i) {
    (animal === 'kitty') ? break:console.log(i);
}


//Ejercicio 8
var value = 1;
switch (value) {
	case 1:
		console.log('I will always run');
	case 2:
		console.log('I will never run');
}

//Ejercicio 9
var animal = 'Lion';
switch (animal) {
  case 'Dog':
    console.log('I will not run since animal !== "Dog"');
    break;
  case 'Cat':
    console.log('I will not run since animal !== "Cat"');
    break;
  default:
    console.log('I will run since animal does not match any other case');
}

//Ejercicio 10
function john() {
  return 'John';
}

function jacob() {
  return 'Jacob';
}

switch (name) {
  case john():
    console.log('I will run if name === "John"');
    break;
  case 'Ja' + 'ne':
    console.log('I will run if name === "Jane"');
    break;
  case john() + ' ' + jacob() + ' Jingleheimer Schmidt':
    console.log('His name is equal to name too!');
    break;
}

//Ejercicio 11
var x = "c";
switch (x) {
	case "a":
	case "b":
	case "c":
		console.log("Either a, b, or c was selected");
	break;
	case "d":
		console.log("Only d was selected");
	break;
	default:
		console.log("No case was matched");
}

//Ejercicio 12
var x = 5 + 7;
console.log(x);

var x = 5 + "7";
console.log(x);

var x = "5" + 7;
console.log(x);

var x = 5 - 7;
console.log(x);

var x = 5 - "7";
console.log(x);

var x = "5" - 7;
console.log(x);

var x = 5 - "x";
console.log(x);

//Ejercicio 13
var a = 'hello' || '';
console.log(a);

var b = '' || [];
console.log(b);

var c = '' || undefined;
console.log(c);

var d = 1 || 5;
console.log(d);

var e = 0 || {};
console.log(e);

var f = 0 || '' || 5;
console.log(f);

var g = '' || 'yay' || 'boo';
console.log(g);

//Ejercicio 14
var a = 'hello' && '';
console.log(a);

var b = '' && [];
console.log(b);

var c = undefined && 0;
console.log(c);

var d = 1 && 5;
console.log(d);

var e = 0 && {};
console.log(e);

var f = 'hi' && [] && 'done' ;
console.log(f);

var g = 'bye' && undefined && 'adios';
console.log(g);

//Ejercicio 15
var foo = function(val) {
	return val || 'default';
}
console.log( foo('burger'));
console.log( foo(100));
console.log( foo([]));
console.log( foo(0));
console.log( foo(undefined));

//ERROR
//Ejercicio 16
var age = 18;
var height = 6;

var isLegal = age >= 18;
var tall = height >= 5.11;
var suitable = isLegal && tall;
var isRoyalty = status === 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterOurBar = suitable || specialCase;

console.log(isLegal);
console.log(tall);
console.log(suitable);
console.log(isRoyalty);
console.log(specialCase);
console.log(canEnterOurBar);

//Ejercicio 17
for (var i = 0; i < 3; i++) {
	if (i === 1) {
		continue;
	}
	console.log(i);
}

//Ejercicio 18
var i = 0;
while (i < 3){
	if (i === 1){
		i = 2;
		continue;
	}
	console.log(i);
	i++;
}

//Ejercicio 19
for(var i = 0; i < 5; i++){
	nextLoop2Iteration:
	for(var j = 0; j < 5; j ++){
		if(i == j) break nextLoop2Iteration;
		console.log(i,j);
	}
}

//Ejercicio 20
function foo() {
	var a = 'hello';

function bar() {
	var b = 'world';
	console.log(a);
	console.log(b);
}

console.log(a);
console.log(b);
}

console.log(a);
console.log(b);

//Ejercicio 21
var nameSum = function sum (a, b) {
	return a + b;
}
var anonSum = function (a, b) {
	return a + b;
}

console.log(nameSum(1, 3));
console.log(anonSum(1, 3));

//Ejercicio 22
var a = [1, 2, 3, 8, 9, 10];
a.slice(0,3).concat([4,5,6,7], a.slice(3,6));
console.log(a.slice(0,3).concat([4,5,6,7], a.slice(3,6)));

var a = [1, 2, 3, 8, 9, 10];
a.slice(3,0, ...[4,5,6,7]);
console.log(a.slice(3,0, ...[4,5,6,7]));

//Ejercicio 23
var array = ['a', 'b', 'c'];

console.log(array.join('->'));
console.log(array.join('.'));
console.log('a.b.c'.split('.'));
console.log('5.4.3.2.1'.split('.'));

//Ejercicio 24
var array = [5, 10, 15, 20, 25];

console.log(Array.isArray(array));
console.log(array.includes(10));
console.log(array.includes(10,2));
console.log(array.includes(25));
console.log(array.lastIndexOf(10, 0));

//Ejercicio 25
var array = ['a', 'b', 'c', 'd', 'e', 'f'];

console.log(array.copyWithin(5, 0, 1));
console.log(array.copyWithin(3, 0, 3));
console.log(array.fill('Z', 0, 5));

var array = ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];

console.log(array.sort());
console.log(array.reverse()); 









